import java.net.*;
import java.io.*;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;

/**
 * Worker class used to perform long running execution of connection to keyboard
 * client and database.
 * @author CapstoneGroup
 *
 */
class Worker extends Thread {
    private static ObjectMapper mapper;
    static{
        mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }
    Socket sock;
    //GestureAnalyzer analyzer;

    Worker (Socket s) { 
    	sock = s; 
    	//analyzer = new GestureAnalyzer("user");
    }

    /**
     * Run method that gets IO streams. Stays connected while controlSwitch is true/
     * when client sends 'quit'.
     */
    public void run(){
        //Get I/O streams in/out from the socket:
        PrintStream out = null;
        BufferedReader in = null;
        String pointInput = "";
        
        try {
            in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            out = new PrintStream(sock.getOutputStream());

            GestureAnalyzer analyzer = null;
            boolean analyzerSet = false;
            
            if (SocketServer.controlSwitch != true) {
                System.out.println("Listener is now shutting down as per client request.");
                out.println("Server is now shutting down. Goodbye!");
            }

            while (!(pointInput.indexOf("quit") > -1)) {
                try {
                    pointInput = in.readLine();
                    if(pointInput.indexOf("quit") > -1){
                        System.out.println("(JAVA): SocketServer recieved message 'quit' from client.");
                        if(analyzer != null){
                            analyzer.finish();
                        }
                        break;
                    }
                    if(analyzer == null){
                        UserInfo uInfo = mapper.readValue(pointInput, UserInfo.class);
                        if(uInfo.uid == 0)
                            continue;
                        else{
                            analyzer = new GestureAnalyzer(uInfo.uid);
                            continue;
                        }
                    }
                    String result = analyzer.recognizeGesture(pointInput, 3);
                    //System.out.println(result);
		    out.print(result);

                    //System.out.println(pointInput);
                    //out.print("Got your input: " + pointInput);
                }
                catch (IOException x) {
                    System.out.println("Server read error.");
                    x.printStackTrace();
                }
            }
            sock.close();
            System.out.println("The Client has left.");
        }
        catch (IOException ioe) { System.out.println(ioe); }

        SocketServer.turnOff();
    }

    
}

/**
* UserInfo class: a basic java bean for jackson JSON conversion.
* client should send a JSON containing the field "userId" and the proper id number
*/
class UserInfo{
    @JsonProperty("userId")
    public int uid;
}

/**
 * SocketServer class establishes a connection with client to send/receive
 * messages.
 * @author CapstoneGroup
 *
 */
public class SocketServer
{
    public static boolean controlSwitch = true;
    private static ServerSocket serverSocket;
    
    public static void main(String a[]) throws IOException
    {
        int q_len = 6;
        int port = 5656;
        Socket sock;
        //set up a server socket that will listen on the specified port
        serverSocket = new ServerSocket(port, q_len);
        System.out.println("Listening for clients @ localhost:" + port);
        
        sock = serverSocket.accept();
        System.out.println("Client connected @ localhost:" + port);
        if(controlSwitch) new Worker(sock).start();

        /* 
        * ***(uncomment for multiple clients--currently not closing correctly)***
        *    //accept a client connection
        *   while(controlSwitch) {
        *        sock = serverSocket.accept();
        *      System.out.println("Client connected @ localhost:" + port);
        *       if(controlSwitch) new Worker(sock).start();
        *   }
        */
    }

    public static void turnOff(){
        try{
            serverSocket.close();
            System.out.println("(JAVA): Server closed. Ending SocketServer.");
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}

/**
 * Exception is an error when comparing shapes of unequal point sizes
 * @author CapstoneGroup
 *
 */
public class ShapeSizeException extends Exception {
	public ShapeSizeException(String str){
		System.err.print(str);
	}
}

/**
 * Exception is a segment of improper value is trying to be used/created.
 * @author CapstoneGroup
 *
 */
public class SegmentException extends Exception {
	public SegmentException(String str){
		System.err.print(str);
	}
}

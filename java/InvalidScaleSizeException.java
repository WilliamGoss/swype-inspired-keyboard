/**
 * Used when an invalid scale size is used.
 * @author CapstoneGroup
 *
 */
public class InvalidScaleSizeException extends Exception {
	public InvalidScaleSizeException(String str){
		System.err.print(str);
	}
}

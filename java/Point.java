/**
 * Point class used to represent an x and y coordinate location of UI. This is used 
 * for mapping out Shapes and their respective segments.
 * @author CapstoneGroup
 *
 */
public class Point{
	private double x;
	private double y;

	/**
	 * Empty Point constructor.
	 */
	public Point(){}

	/**
	 * Point constructor that sets x and y values of Point.
	 * @param _x - x coordinate
	 * @param _y - y coordinate
	 */
	public Point(double _x, double _y){
		x = _x;
		y = _y;
	}

	/**
	 * Gets X axis of Point object.
	 * @return x value of this Point
	 */
	public double getX(){
		return x;
	}

	/**
	 * Sets X axis of Point object.
	 * @param newX - value of X
	 */
	public void setX(double newX){
		this.x = newX;
	}

	/**
	 * Gets Y axis of Point object.
	 * @return y value of this Point
	 */
	public double getY(){
		return y;
	}

	/**
	 * Sets Y axis of Point object.
	 * @param newY - value of Y
	 */
	public void setY(double newY){
		y = newY;
	}

	/**
	 * Prints x and y coordinates of Point.
	 * @return String x and y
	 */
	public String print(){
		return "("+x+", "+y+")\n";
	}
}
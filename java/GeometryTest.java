import java.util.ArrayList;

/**
 * Main class used for testing the effectiveness of the GestureAnalyzer.
 * <p>GestureAnaylzer object is created. It is then passed a String representation of a JSON object. This is then
 * called by the GestureAnalyzer's recognizeGesture() method. A list of potential dictionary words to match the
 * sokgraph passed within the JSON is then printed.</p>
 * @author CapstoneGroup
 *
 */
public class GeometryTest{
	public static void main(String[] args){
		//yes this name is a joke
		GestureAnalyzer analGest = new GestureAnalyzer(3);

		String callJSON = "{\"points\": [{\"y\": 794, \"x\": 257}, {\"y\": 785, \"x\": 240}, {\"y\": 775, \"x\": 223}, {\"y\": 766, \"x\": 206}, {\"y\": 756, \"x\": 189}, {\"y\": 747, \"x\": 172}, {\"y\": 737, \"x\": 154}, {\"y\": 728, \"x\": 137}, {\"y\": 718, \"x\": 120}, {\"y\": 709, \"x\": 103}, {\"y\": 699, \"x\": 86}, {\"y\": 689, \"x\": 68}, {\"y\": 689, \"x\": 114}, {\"y\": 689, \"x\": 160}, {\"y\": 689, \"x\": 206}, {\"y\": 689, \"x\": 252}, {\"y\": 689, \"x\": 298}, {\"y\": 689, \"x\": 343}, {\"y\": 689, \"x\": 389}, {\"y\": 689, \"x\": 435}, {\"y\": 689, \"x\": 481}, {\"y\": 689, \"x\": 527}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}, {\"y\": 689, \"x\": 572}]}";
		
		System.out.println(analGest.recognizeGesture(callJSON, 10));
	}
}
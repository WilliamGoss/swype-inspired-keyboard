# README #

### What is this repository for? ###

* This repository contains the work that was done on a project we were assigned for CSC394 at DePaul University. The goal was to create a keyboard input system similar to that of Swype. The front-end of this project was done in Python using the PyGame engine to track mouse movement and actions. The information was stored and transferred to a Java server through a JSON message. The information was extracted and the best possible word choice was chosen via an algorithm to measure various angles and distances. After gathering relevant information, a database was queried and the top three word choices were sent back to the user to choose from. Information is stored via database and users are able to create profiles and track their usage.
* Version 1.0

### Who do I talk to? ###

* http://www.linkedin.com/in/craigbrott
* http://www.linkedin.com/pub/michael-meidl/64/33b/497
* http://www.linkedin.com/in/williamtgoss/
import os
import thread
from fabric.api import local, lcd
import platform
import threading
import time

## Compiles the Java server.
def compileGestureAnalyzer():
    with lcd("java"):
        local("javac -d bin -cp \"lib/*\" *.java")

## Defines a subclass of Thread that runs the SocketServer.
class RunSocketServer (threading.Thread):
    ## 
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    
    ## 
    def run(self):
        with lcd("java"):
            sys = platform.system()
            if sys == "Windows":
                local("java -cp \"bin;lib/*\" SocketServer")
            elif sys == "Linux" or sys == "Darwin":
                local("java -cp \"bin:lib/*\" SocketServer")

## Defines a subclass of Thread that runs the keyboard.
class RunKeyboard (threading.Thread):
    ## 
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    
    def run(self):
        with lcd("../python"):
            local("python keyboard.py")

## Runs the whole system.
def run():
    compileGestureAnalyzer()
    
    #create new threads
    server = RunSocketServer(1, "SocketServer", 1)
    client = RunKeyboard(2, "Keyboard", 2)
    
    #start server thread
    server.start()
    
    #sleep for 5 seconds while server sets up
    time.sleep(3)
    
    #start client thread
    client.start()

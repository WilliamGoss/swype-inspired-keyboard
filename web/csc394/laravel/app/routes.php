<?php

Route::get('join',  array('uses' => 'UserController@getCreateUser'));
Route::post('join',  array('before' => 'csrf', 'uses' => 'UserController@validateCreateUser'));

Route::get('login',  array('uses' => 'UserController@getLoginUser'));
Route::post('login',  array('before' => 'csrf', 'uses' => 'UserController@validateLoginUser'));

Route::get('/',  array('before' => 'auth', 'uses' => 'UserController@getDashboard'));

Route::post('/change-eula',  array('before' => 'auth|csrf', 'uses' => 'UserController@validateChangeEULAAnswer'));

Route::post('/change-settings',  array('before' => 'auth|csrf', 'uses' => 'UserController@validateChangeSettings'));

Route::post('/delete-account',  array('before' => 'auth|csrf', 'uses' => 'UserController@validateDeleteAccount'));


Route::get('logout',  array('before' => 'auth', 'uses' => 'UserController@logoutUser'));
@if (Session::get("message") != null)
    <div class="alert alert-danger">
        {{ Session::get("message") }}
    </div>
@endif

{{ Form::open($attributes = array("role" => "form", "url" => "/login")) }}
    <div class="form-group">
        {{ Form::label("username") }}
        {{ Form::text("username", null, array("class" => "form-control", "placeholder" => "Username")) }}
    </div>
    <div class="form-group">
        {{ Form::label("password") }}
        {{ Form::password("password", array("class" => "form-control", "placeholder" => "Password")) }}
    </div>
    {{ Form::submit("Login", array("class" => "btn btn-primary")) }}
{{ Form::close() }}

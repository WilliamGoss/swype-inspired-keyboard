@if (Session::get("message") != null)
    <div class="alert alert-danger">
        {{ Session::get("message") }}
    </div>
@endif

@if ($errors->getMessages() != null)
    <div class="alert alert-danger">
    @foreach ($errors->getMessages() as $error) 
        <p>
            {{ $error[0] }}
        </p>
    @endforeach
    </div>
@endif

{{ Form::open($attributes = array("role" => "form", "url" => "/join")) }}
    <div class="form-group">
        {{ Form::label("username", null, array("class" => "control-label")) }}
        {{ Form::text("username", null, array("class" => "form-control", "placeholder" => "Username")) }}
    </div>
    <div class="form-group">
        {{ Form::label("password", null, array("class" => "control-label")) }}
        {{ Form::password("password", array("class" => "form-control", "placeholder" => "Password")) }}
    </div>
    <div class="form-group">
        {{ Form::label("password_confirmation", "Confirm Pasword", array("class" => "control-label")) }}
        {{ Form::password("password_confirmation", array("class" => "form-control", "placeholder" => "Confirm Password")) }}
    </div>
    {{ Form::submit("Create Account", array("class" => "btn btn-primary")) }}
{{ Form::close() }}
<br>

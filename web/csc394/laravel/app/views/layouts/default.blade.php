<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{ HTML::style('//fonts.googleapis.com/css?family=Quicksand:300') }}
        {{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css') }}
        {{ HTML::style('/css/style.css') }} 
        <title>Team 4 - {{ $pageTitle }}</title>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><b>Team 4 Swipers</b></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::check())
                            <li><a href="/logout">Logout</a></li>
                        @else
                            <li><a href="/login">Login</a></li>
                            <li><a href="/join">Join</a></li>
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <h2>{{ $pageTitle }}</h2>
                    <p>{{ $pageDescription }}</p><br>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    {{ $content }}
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>

        {{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
        {{ HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js') }}
        <script type="text/javascript">
            $(document).ready(function() {
                $('#tab1 a').click(function (e) {
                    e.preventDefault()
                    $(this).tab('show')
                });
                $('#tab2 a').click(function (e) {
                  e.preventDefault()
                  $(this).tab('show')
                });
                $('#tab3 a').click(function (e) {
                  e.preventDefault()
                  $(this).tab('show')
                });
            });
        </script>
    </body>
</html>


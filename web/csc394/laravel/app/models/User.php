<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 * @var array
	 */
	protected $hidden = array('password');
    
	/**
	 * The attributes that cannot be mass-written.
	 * @var array
	 */
    protected $guarded = array();

	/**
	 * Get the unique identifier for the user.
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 * @return string
	 */
	public function getAuthPassword() {
		return $this->password;
	}
    
	/**
	 * Get the e-mail address where password reminders are sent.
	 * @return string
	 */
	public function getReminderEmail() {
		return $this->email;
	}
    
    public function getUserStats($userId) {
        $wordsCompleted = $this->select("words_completed")->where("id", "=", $userId)->get();
        $keystrokesSaved = $this->select("keystrokes_saved")->where("id", "=", $userId)->get();
        
        $numWordsCompletedScoreLower = $this->where("words_completed", "<", $wordsCompleted->first()->words_completed)->count();
        $numKeystrokesSavedScoreLower = $this->where("keystrokes_saved", "<", $keystrokesSaved->first()->keystrokes_saved)->count();   
        
        $userCount = $this->count();
        
        return array(
            "avgWordsCompletedPercentile" => round(($numWordsCompletedScoreLower/$userCount) * 100),
            "avgKeystrokesSavedPercentile" => round(($numKeystrokesSavedScoreLower/$userCount) * 100)
        );
    }
}
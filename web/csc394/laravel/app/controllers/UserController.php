<?php

class UserController extends BaseController {
    
    protected $layout = 'layouts.default';
    
	public function getDashboard() {
        $this->layout->content = View::make('user.dashboard');
        $this->layout->pageTitle = "Dashboard";
        $this->layout->pageDescription = "Use this dashboard to manage your account and view your usage statistics.";
        
        $User = new User;
        
        $this->layout->content->user = Auth::user();
        $this->layout->content->userStats = $User->getUserStats(Auth::user()->id);
	}
    
	public function getLoginUser() {
        $this->layout->content = View::make('user.login');
        $this->layout->pageTitle = "Log In";
        $this->layout->pageDescription = "Log in below using your email address and password. If you do not have an account, you can <a href=\"/join\">create one</a>.";
	}
    
    public function validateLoginUser() {
        $input = Input::all();
        
        //attempt to log user in
        if (Auth::attempt(array('username' => $input["username"], 'password' => $input["password"]))) {
            //redirect to the user's intended page, and if that page is unavailable then redirect to home
            return Redirect::intended('/');
        }
        else {
            return Redirect::to('/login')->withInput($input)->with('message', 'Invalid username or password.');
        }
    }
    
	public function logoutUser() {
        Auth::logout();
        return Redirect::to('/');
	}
    
	public function getCreateUser() {
        $this->layout->content = View::make('user.join');
        $this->layout->pageTitle = "Join";
        $this->layout->pageDescription = "Enter a username and password below to create an account.";
	}
    
    public function validateCreateUser() {
        //validation rules
        $rules = array(
            'username'              => 'Required|Between:3,64|AlphaNum|Unique:users',
            'password'              => 'Required|AlphaNum|Min:6|Confirmed',
            'password_confirmation' => 'Required|AlphaNum|Min:6'
        );
        
        //construct a new validator
        $validator = Validator::make(Input::all(), $rules);
        
        //validate input
        if(!$validator->passes()) {
            //validation failure
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        //validation success, these are the fields we want to insert into the database
        $queryData = array(
          "username"    => Input::get("username"),
          "password"    => Hash::make(Input::get("password"))
        );
        
        //set up new user model
        $user = new User;
        
        //attempt to create a new user
        if (!$user->create($queryData)) {
            //failure, redirect back with error message and inputs
            return Redirect::back()
                ->with('message', 'Something went wrong while creating your account. Please try again.')
                ->with('message-class', 'alert-danger')
                ->withInput();
        }
        
        //success, attempt to login new user and redirect to dashboard
        if(Auth::attempt(Input::only("username", "password"))) {
            //login success
            return Redirect::to('/')->with('message', 'Thanks for joining!')
            ->with('message-class', 'alert-success');
        }
        else {
            //login failure, let them try again
            return Redirect::to('/login');
        }
    }
    
    public function validateChangeSettings() {
        $user = Auth::user();
        
        //validation rules
        $rules = array(
            'password'              => 'required_with:password_confirmation | alphanum | min:6 | confirmed',
            'password_confirmation' => 'required_with:password | alphanum | min:6'
        );
        
        //construct the username validator and add it to the rules
        $currentUsername = Auth::user()->username;
        $newUsername = trim(Input::get("username"));
        
        if($currentUsername == $newUsername) {
            $rules["username"] = 'required | between:3,64 | alphanum';
        }
        else {
            $rules["username"] = 'required | between:3,64 | alphanum | unique:users,username';
        }
        
        //construct a new validator
        $validator = Validator::make(Input::all(), $rules);
        
        //validate input
        if(!$validator->passes()) {
            //validation failure
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        //validation success, these are the fields we want to insert into the database
        $queryData = array(
            "username"    => Input::get("username"),
            "password"    => Hash::make(Input::get("password"))
        );
        
        //run query
        try {
            if (!$user->update($queryData)) {
                return Redirect::back()
                    ->with('message', 'Something went wrong. Please try again.')
                    ->with('message-class', 'alert-danger');
            }
        } catch(\Exception $e) {
            return Redirect::back()
                ->with('message', 'Something went wrong. Please try again.')
                ->with('message-class', 'alert-danger'); 
        }
        
        return Redirect::to('/')
            ->with('message', 'You updated your profile!')
            ->with('message-class', 'alert-success');
    }
    
    public function validateChangeEULAAnswer() {
        $user = Auth::user();
        
        //validation rules
        $rules = array(
            'acceptedEULA'      => 'accepted'
        );
        
        $messages = array(
            'acceptedEULA.accepted' => 'You must accept the terms of the End User License Agreement.'
        );
        
        //construct a new validator
        $validator = Validator::make(Input::only("acceptedEULA"), $rules, $messages);
        
        //validate input
        if(!$validator->passes()) {
            //validation failure
            return Redirect::back()->withErrors($validator)->withInput();
        }
        
        //validation success, these are the fields we want to insert into the database
        //apply updates
        if (!$user->update(Input::only("acceptedEULA"))) {
            return Redirect::back()
                ->with('message', 'Something went wrong. Please try again.')
                ->with('message-class', 'alert-danger')
                ->withInput();
        }
        
        return Redirect::to('/')
            ->with('message', 'You updated your profile!')
            ->with('message-class', 'alert-success');
    }
}
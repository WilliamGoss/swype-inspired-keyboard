from fabric.api import *

## Environment variables
env.user = "ubuntu"
env.hosts = ["54.84.205.6"]

remoteWebRoot = "/var/www/"
site = "csc394"

## Stages project for testing on server before full deployment
def stage():
    print "staging"

## Archives, uploads, and deploys a laravel project.
def deploy():
    #archive the project ready for deployment
    local("rm -f " + site + ".tar.gz")
    local("tar -czf " + site + ".tar.gz " + site)

    #upload the archive to the server
    put(site + ".tar.gz", remoteWebRoot + site + ".tar.gz", use_sudo = True)
    
    #unpack and set up new version of website
    with cd(remoteWebRoot):
        sudo("rm -rf " + site)
        sudo("mkdir " + site)
        sudo("tar xzf " + site + ".tar.gz")
        sudo("chown -R www-data:www-data " + site)
        sudo("chmod -R 744 " + site)
  
drop database if exists shark2;

create database shark2 character set utf8 collate utf8_unicode_ci;

use shark2;

drop table if exists users;
drop table if exists sokgraphs;
drop table if exists user_sokgraphs;

## Keeps track of user accounts.
create table users(
    id int(10) unsigned not null auto_increment,
    username varchar(255) not null default '',
    password varchar(255) not null default '',
	unhashed_password varchar(255) not null default '',
	acceptedEULA bool not null default false,
	words_completed int(10) unsigned not null default 0,
	keystrokes_saved int(10) unsigned not null default 0,
	created_at datetime,
	updated_at datetime,

    unique(username),
	primary key (id)
);

create table sokgraphs(
    id int(10) unsigned not null auto_increment,
    word varchar(255) not null default '',
    encoded_sokgraph varchar(10000) not null default '',

	primary key (id)
);

create table user_sokgraphs(
	id int(10) unsigned not null auto_increment,
    user_gesture varchar(10000) not null default '',
	recognition_results varchar(1024) not null default '',
	user_id int(10) unsigned not null,

	primary key (id),
	foreign key (user_id) references users (id)
);

## Data updates
insert into users (username, password, unhashed_password, created_at, updated_at) values ('craig', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('will', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('michael', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('alex', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('kenneth', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('bill', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('neil', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('thomas', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());
insert into users (username, password, unhashed_password, created_at, updated_at) values ('robert', '$2y$10$c6oTdlCBr6EoByWgTSOR/uu4awywXdxusEuDTyKOR7a6hVHqCTPyq', 'capstone', now(), now());

update users set words_completed = 500, keystrokes_saved = 200 where id = 1;
update users set words_completed = 232, keystrokes_saved = 104 where id = 2;
update users set words_completed = 54, keystrokes_saved = 253 where id = 3;
update users set words_completed = 482, keystrokes_saved = 381 where id = 4;
update users set words_completed = 492, keystrokes_saved = 38 where id = 5;
update users set words_completed = 238, keystrokes_saved = 593 where id = 6;
update users set words_completed = 284, keystrokes_saved = 389 where id = 7;
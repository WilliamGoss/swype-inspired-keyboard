#Retrieves list of all users
select username from users;

#Retrieves list of all user’s custom words (dictionary)
select word from words_sokgraphs_stats
join users on users.id = words_sokgraphs_stats.user_id;

#Retrieves list of all words (dictionary)
select word from words;

#Add a correction
insert into corrections values (:sokgraph_id, :incorrect_word_id, :user_id, null, null);

#Deleting User
delete from users where id=:id;

#Add a User
insert into user (username, password) values (:username, :password);

#Add word to user dictionary
insert into words (word) values (:word);

#Update User
update users set username=:username where id=:id;

#Delete a word from user dictionary
delete from words where id=:id;
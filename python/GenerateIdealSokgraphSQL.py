import json
import csv
from math import ceil
import numpy as np
import pandas

#holds a dictionary containing each letter key on the keyboard and its coordinates for generating ideal sokgraphs
keyRectCoords = {}
keyRectSize = (27, 42)

## Load in the key coordinates and key size from the key_data.csv
def getKeyData():
    #read in the key coordinates from the key_data.csv file and add the coordinates of all letter keys to the keyRectCoords dictionary
    with open('io/key_data.csv', 'rb') as csvfile:
        csvReader = csv.reader(csvfile)

        #skip the header row
        next(csvReader, None)

        #for each key definition, generate an (x,y) coordinate tuple
        for row in csvReader:
            keyRectCoords[row[0]] = (int(row[1]), int(row[2]))

## Collect the list of words from words.txt and output ideal.json full of ideal sokgraphs for each word.
def getWordListAndMakeIdealSokgraphs(keyRectCoords, keyRectSize):
    #read in list of words and generate ideal sokgraphs for them
    file = open('io/words.txt', 'r')
    words = []
    counter = 0
    for line in file:
        if counter > 999:
            break
        words.append(line.strip().lower().replace("'", "").replace("\"", ""))
        counter = counter + 1
    
    #convert words to a set and then back to a list to remove duplicates
    words = list(set(words))
    
    fileOut = open('io/ideal.json', 'wb')
    finalOutput = []
    for word in words:
        output = []
        for point in generateIdealSokgraph(word, keyRectCoords, keyRectSize):
            output.append({"x" : point[0], "y" : point[1]})
        finalOutput.append({"label" : word, "shape" : output})
    fileOut.write(json.dumps(finalOutput));

## Takes a single word and outputs an ideal sokgraph for it.
def generateIdealSokgraph(word, keyRectCoords, keyRectSize):
    #these two lists hold the (x,y) centers of each letter key in the word, in order
    xx = []
    yy = []
    
    #counter and word length for the following loop
    counter = 1
    wordLength = len(word)
    
    #get the (x,y) center of each letter key in the word and add it to the list of centers
    for letter in list(word):
        x = keyRectCoords[letter][0] + (keyRectSize[0] / 2)
        y = keyRectCoords[letter][1] + (keyRectSize[1] / 2)
        
        #append the x-coordinate component of the center along with n nans that will be interpoalted later
        xx.append(x)
        #don't add NaN values to interpolate if it's the last letter of the word
        if counter < wordLength:
            for i in range(0, 10):
                xx.append(np.nan)
        #append the y-coordinate component of the center along with n nans that will be interpoalted later
        yy.append(y)
        #don't add NaN values to interpolate if it's the last letter of the word
        if counter < wordLength:
            for i in range(0, 10):
                yy.append(np.nan)
        
        counter = counter + 1
    
    #perform interpolation
    dataFrame = pandas.DataFrame(data={'x' : xx, 'y' : yy})
    interpolated = dataFrame.apply(pandas.Series.interpolate)
    
    result = []
    
    #get the numpy array back into a list of cooridnate tuples
    for r in range(0, len(interpolated['x'])):
        #convert each coordinate component back to an integer
        newX = int(ceil(interpolated['x'][r]))
        newY = int(ceil(interpolated['y'][r]))
        result.append((newX, newY))
    
    return result

## 
def generateSQL():
    #list that will contain the insert statements
    sql = []
    
    #read in ideal.json
    with open("io/ideal.json") as json_data:
        d = json.load(json_data)
    
        #for each word/sokgraph pair compose an insert statement
        for i in d:
             s = "insert into sokgraphs (word, encoded_sokgraph) values ('" + i["label"] + "', " + "'" + json.dumps({"points": i["shape"]}) + "');\n"
             sql.append(s)

    #write the insert statements out to ideal.sql
    with open("io/ideal.sql", "w") as output:
        for x in sql:
            output.write(x)

## Main method.
if __name__ == "__main__":
    #get the key data from the key_data.csv file
    getKeyData()
    
    #generate ideal sokgraphs for each word (converts: words.txt -> ideal.json)
    getWordListAndMakeIdealSokgraphs(keyRectCoords, keyRectSize)
    
    #genertate a runnable sql file to insert all the words and their ideal sokgraphs into the database (converts: ideal.json -> ideal.sql)
    generateSQL()
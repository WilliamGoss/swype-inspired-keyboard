import pygame
import sys
import socket
import json
import csv
from option import Option

## Holds the client socket data.
clientSocket = None

## Global variable for word choices
wordchoice1 = ""
wordchoice2 = ""
wordchoice3 = ""
wordsSent = False
screen = pygame.display.set_mode((320, 480))
shiftCheck = False

## Prints the value of a single key when pressed.
def printKey(key):
    global my_string
    global shiftCheck
    if shiftCheck:
        my_string += key.upper()
        shiftCheck = False
    else:
        my_string += key

## Sends a user id json
def sendUserId(uid):
    #use the global clientSocket variable
    global clientSocket

    sendData = json.dumps({"userId" : uid}) + "\n"
    clientSocket.sendall(sendData)

## Takes a sokgraph (in the form of a list of tuples where tuples are (x,y) coordinate pairs).
def sendSokgraph(sokgraph):
    #use the global clientSocket variable
    global clientSocket
    output = []
    for point in sokgraph:
        output.append({"x" : point[0], "y" : point[1]})
    
    sendData = json.dumps({"points" : output}) + "\n"
    clientSocket.sendall(sendData)

## Receives data and decodes the json into the appropriate python struct.
def receiveData():
    #use the global clientSocket variable
    global clientSocket
    
    receiveData = clientSocket.recv(1024)
    x = len(receiveData)
    rd = receiveData[0:x-4] + receiveData[x-2:]
    data = json.loads(rd)
    global wordsSent
    wordsSent  = True
    global wordchoice1
    wordchoice1  = data["results"][0]["label"]
    global wordchoice2
    wordchoice2  = data["results"][1]["label"]
    global wordchoice3
    wordchoice3  = data["results"][2]["label"]


## Word wrapper
def render_textrect(string, font, rect, text_color, background_color, justification=0): 
    final_lines = []

    requested_lines = string.splitlines()

    global screen

    # Create a series of lines that will fit on the provided
    # rectangle.

    for requested_line in requested_lines:
        if font.size(requested_line)[0] > rect.width:
            words = requested_line.split(' ')
            # if any of our words are too long to fit, return.
            for word in words:
                if font.size(word)[0] >= rect.width:
                    print ("The word " + word + " is too long to fit in the rect passed.")
            # Start a new line
            accumulated_line = ""
            for word in words:
                test_line = accumulated_line + word + " "
                # Build the line while the words fit.    
                if font.size(test_line)[0] < rect.width:
                    accumulated_line = test_line 
                else: 
                    final_lines.append(accumulated_line) 
                    accumulated_line = word + " " 
            final_lines.append(accumulated_line)
        else: 
            final_lines.append(requested_line) 

    accumulated_height = 0 
    for line in final_lines: 
        if accumulated_height + font.size(line)[1] >= rect.height:
            print ("Once word-wrapped, the text string was too tall to fit in the rect.")
        if line != "":
            tempsurface = font.render(line, 0, text_color)
            if justification == 0:
                screen.blit(tempsurface, (13, accumulated_height + 101))
            elif justification == 1:
                screen.blit(tempsurface, (((rect.width - tempsurface.get_width()) / 2), accumulated_height))
            elif justification == 2:
                screen.blit(tempsurface, (rect.width - tempsurface.get_width(), accumulated_height))
            else:
                print ("Invalid justification argument: " + str(justification))
        accumulated_height += font.size(line)[1]           
    
## Handles pygame and the keyboard input.
def keyboard():
    #initialize pygame
    pygame.init()
    
    #make the window with the keyboard background
    #width, height = 640, 960
    #screen = pygame.display.set_mode((width, height))
    global screen
    background = pygame.image.load("../img/keyboard-half-final.png").convert()

    #transparency boxes
    keyRectSize = (27, 42)
    rect = pygame.Surface(keyRectSize, pygame.SRCALPHA, 32)
    rect.fill((0, 0, 0, 0))
    spaceRect = pygame.Surface((156, 42), pygame.SRCALPHA, 32)
    spaceRect.fill((0, 0, 0, 0))
    wordChoiceRect = pygame.Surface((96, 30), pygame.SRCALPHA, 32)
    wordChoiceRect.fill((0, 0, 0, 0))
    bigkeyRectSize = (42, 42)
    bigKeyRect = pygame.Surface(bigkeyRectSize, pygame.SRCALPHA, 32)
    bigKeyRect.fill((0, 0, 0, 0))
    logOutRectSize = (90, 30)
    logOutRect = pygame.Surface(logOutRectSize, pygame.SRCALPHA, 32)
    logOutRect.fill((0, 0, 0, 0))
    
    #draw the keyboard background
    screen.blit(background, (0,0))
    
    #holds a dictionary containing each key on the keyboard and its coordinates for generating ideal sokgraphs
    keyRectCoords = {}
    
    #read in the key coordinates from the key_data.csv file and add the coordinates of all letter keys to the keyRectCoords dictionary
    with open('io/key_data.csv', 'rb') as csvfile:
        csvReader = csv.reader(csvfile)

        #skip the header row
        next(csvReader, None)

        #for each key definition, generate an (x,y) coordinate tuple
        for row in csvReader:
            keyRectCoords[row[0]] = (int(row[1]), int(row[2]))
    
    #add the coordinates of the remaining non-letter keys to the keyRectCoords dictionary
    keyRectCoords['space'] = (82, 429)
    keyRectCoords['choice1'] = (14, 230)
    keyRectCoords['choice2'] = (113, 230)
    keyRectCoords['choice3'] = (213, 230)
    keyRectCoords['backspace'] = (273, 377)
    keyRectCoords['shift'] = (6, 377)
    keyRectCoords['logout'] = (230, 0)
    
    #draw all of the key boxes
    q = screen.blit(rect, keyRectCoords['q'])
    w = screen.blit(rect, keyRectCoords['w'])
    e = screen.blit(rect, keyRectCoords['e'])
    r = screen.blit(rect, keyRectCoords['r'])
    t = screen.blit(rect, keyRectCoords['t'])
    y = screen.blit(rect, keyRectCoords['y'])
    u = screen.blit(rect, keyRectCoords['u'])
    i = screen.blit(rect, keyRectCoords['i'])
    o = screen.blit(rect, keyRectCoords['o'])
    p = screen.blit(rect, keyRectCoords['p'])
    a = screen.blit(rect, keyRectCoords['a'])
    s = screen.blit(rect, keyRectCoords['s'])
    d = screen.blit(rect, keyRectCoords['d'])
    f = screen.blit(rect, keyRectCoords['f'])
    g = screen.blit(rect, keyRectCoords['g'])
    h = screen.blit(rect, keyRectCoords['h'])
    j = screen.blit(rect, keyRectCoords['j'])
    k = screen.blit(rect, keyRectCoords['k'])
    l = screen.blit(rect, keyRectCoords['l'])
    z = screen.blit(rect, keyRectCoords['z'])
    x = screen.blit(rect, keyRectCoords['x'])
    c = screen.blit(rect, keyRectCoords['c'])
    v = screen.blit(rect, keyRectCoords['v'])
    b = screen.blit(rect, keyRectCoords['b'])
    n = screen.blit(rect, keyRectCoords['n'])
    m = screen.blit(rect, keyRectCoords['m'])
    space = screen.blit(spaceRect, keyRectCoords['space'])
    wordbox1 = screen.blit(wordChoiceRect, keyRectCoords['choice1'])
    wordbox2 = screen.blit(wordChoiceRect, keyRectCoords['choice2'])
    wordbox3 = screen.blit(wordChoiceRect, keyRectCoords['choice3'])
    backspace = screen.blit(bigKeyRect, keyRectCoords['backspace'])
    shift = screen.blit(bigKeyRect, keyRectCoords['shift'])
    logout = screen.blit(logOutRect, keyRectCoords['logout'])
    
    #get the blank word choices and put them in options
    global wordchoice1
    global wordchoice2
    global wordchoice3

    option = [Option(wordchoice1, (14,230)), Option(wordchoice2, (113, 230)),\
	Option(wordchoice3, (213, 230))]

    my_font = pygame.font.Font("../misc/open-sans/OpenSans-Regular.ttf", 14)

    #username
    global my_string
    userLoggedIn = "@" + my_string
    my_string = ""
    loggedInUser = my_font.render(userLoggedIn, 0, (0, 0, 0))
    screen.blit(loggedInUser, (5, 6)) 

    #logout text
    logOutText = "Sign Out"
    logOutUser = my_font.render(logOutText, 0, (0, 0, 0))
    screen.blit(logOutUser, (245, 4))

    #output box
    my_rect = pygame.Rect((40, 40, 295, 125))
    rendered_text = render_textrect(my_string, my_font, my_rect, (0, 0, 0), (0, 0, 0), 0)
    
    wordPicked = False
    global shiftCheck 
    
    pygame.display.update()

    points = []
    #mouse down position
    mdpos = None
    #if the cursor goes outside the key area, it goes to true
    garbageFlag = False
    
    while True:
        xaxis, yaxis = pygame.mouse.get_pos()
        pygame.display.flip()
        
        for event in pygame.event.get():
            drawing = []
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                dpos = pygame.mouse.get_pos()
                if event.pos[1] <= 262:
                    garbageFlag = True
                mdpos = event.pos
            elif event.type == pygame.MOUSEBUTTONUP:
                upos = pygame.mouse.get_pos()
                mdpos = None
                screen.fill([0, 0, 0])
                screen.blit(background, (0,0))
                if len(points) > 1:
                    sendSokgraph(points)
		    receiveData()
            	    global wordsSent
		    if wordsSent == True:
			options = [Option(wordchoice1, (14, 230)), Option(wordchoice2, (114, 230)), Option(wordchoice3, (214, 230))]
                    points = []
                garbageFlag = False
		if (wordbox1.collidepoint(dpos) and wordbox1.collidepoint(upos)) or (wordbox2.collidepoint(dpos) and wordbox2.collidepoint(upos)) or (wordbox3.collidepoint(dpos) and wordbox3.collidepoint(upos)):
		    if wordbox1.collidepoint(upos) and len(wordchoice1) > 0:
                            wc = str(wordchoice1)
                            if len(my_string) == 0:
                                wc = wc[0].upper() + wc[1:]
                                my_string += wc
                            else:
                                if shiftCheck:
                                    my_string += " " + wc[0].upper() + wc[1:]
                                else:
                                    my_string += " " + wc
			    wordPicked = True
		    elif wordbox2.collidepoint(upos) and len(wordchoice2) > 0:
                            wc = str(wordchoice2)
                            if len(my_string) == 0:
                                wc = wc[0].upper() + wc[1:]
                                my_string += wc
                            else:
                                if shiftCheck:
                                    my_string += " " + wc[0].upper() + wc[1:]
                                else:
                                    my_string += " " + wc
			    wordPicked = True
		    elif wordbox3.collidepoint(upos) and len(wordchoice3) > 0:
			    wc = str(wordchoice3)
                            if len(my_string) == 0:
                                wc = wc[0].upper() + wc[1:]
                                my_string += wc
                            else:
                                if shiftCheck:
                                    my_string += " " + wc[0].upper() + wc[1:]
                                else:
                                    my_string += " " + wc
			    wordPicked = True	    
                if q.collidepoint(dpos) and q.collidepoint(upos):
                    printKey("q")
                if w.collidepoint(dpos) and w.collidepoint(upos):
                    printKey("w")
                if e.collidepoint(dpos) and e.collidepoint(upos):
                    printKey("e")
                if r.collidepoint(dpos) and r.collidepoint(upos):
                    printKey("r")
                if t.collidepoint(dpos) and t.collidepoint(upos):
                    printKey("t")
                if y.collidepoint(dpos) and y.collidepoint(upos):
                    printKey("y")
                if u.collidepoint(dpos) and u.collidepoint(upos):
                    printKey("u")
                if i.collidepoint(dpos) and i.collidepoint(upos):
                    printKey("i")
                if o.collidepoint(dpos) and o.collidepoint(upos):
                    printKey("o")
                if p.collidepoint(dpos) and p.collidepoint(upos):
                    printKey("p")
                if a.collidepoint(dpos) and a.collidepoint(upos):
                    printKey("a")
                if s.collidepoint(dpos) and s.collidepoint(upos):
                    printKey("s")
                if d.collidepoint(dpos) and d.collidepoint(upos):
                    printKey("d")
                if f.collidepoint(dpos) and f.collidepoint(upos):
                    printKey("f")
                if g.collidepoint(dpos) and g.collidepoint(upos):
                    printKey("g")
                if h.collidepoint(dpos) and h.collidepoint(upos):
                    printKey("h")
                if j.collidepoint(dpos) and j.collidepoint(upos):
                    printKey("j")
                if k.collidepoint(dpos) and k.collidepoint(upos):
                    printKey("k")
                if l.collidepoint(dpos) and l.collidepoint(upos):
                    printKey("l")
                if z.collidepoint(dpos) and z.collidepoint(upos):
                    printKey("z")
                if x.collidepoint(dpos) and x.collidepoint(upos):
                    printKey("x")
                if c.collidepoint(dpos) and c.collidepoint(upos):
                    printKey("c")
                if v.collidepoint(dpos) and v.collidepoint(upos):
                    printKey("v")
                if b.collidepoint(dpos) and b.collidepoint(upos):
                    printKey("b")
                if n.collidepoint(dpos) and n.collidepoint(upos):
                    printKey("n")
                if m.collidepoint(dpos) and m.collidepoint(upos):
                    printKey("m")
                if space.collidepoint(dpos) and space.collidepoint(upos):
                    printKey(" ")
                if backspace.collidepoint(dpos) and backspace.collidepoint(upos):
                    if len(my_string) > 0:
                        my_string = my_string[:-1]
                if shift.collidepoint(dpos) and shift.collidepoint(upos):
                    shiftCheck = True
                if logout.collidepoint(dpos) and logout.collidepoint(upos):
                    pygame.quit()
                    print "(PYTHON): Quitting keyboard"
                    socketQuit()
                    return "Done"
            elif mdpos and event.type == pygame.MOUSEMOTION:
                if (event.pos[1] >= 264):
                    points.append(event.pos)
                    r1 = pygame.draw.aaline(screen, (255, 0, 0), mdpos, event.pos, 10)
                    r2 = pygame.draw.aaline(screen, (255, 0, 0), (mdpos[0]+1, mdpos[1]+1), (event.pos[0]+1, event.pos[1]+1), 10)
                    r3 = pygame.draw.aaline(screen, (255, 0, 0), (mdpos[0]+2, mdpos[1]+2), (event.pos[0]+2, event.pos[1]+2), 10)
                    r4 = pygame.draw.aaline(screen, (255, 0, 0), (mdpos[0]-1, mdpos[1]-1), (event.pos[0]-1, event.pos[1]-1), 10)
                    r5 = pygame.draw.aaline(screen, (255, 0, 0), (mdpos[0]-2, mdpos[1]-2), (event.pos[0]-2, event.pos[1]-2), 10)
                    drawing.append(r1)
                    drawing.append(r2)
                    drawing.append(r3)
                    drawing.append(r4)
                    drawing.append(r5)
                else:
                    garbageFlag = True
                mdpos = event.pos
            if wordPicked == True:
		  wordchoice1 = ""
		  wordchoice2 = ""
		  wordchoice3 = ""
		  options = [Option(wordchoice1, (14, 230)), Option(wordchoice2,(114,230)), Option(wordchoice3, (214, 230))]
		  wordPicked = False
            rendered_text = render_textrect(my_string, my_font, my_rect, (0, 0, 0), (0, 0, 0), 0)       
            screen.blit(loggedInUser, (5, 6))
            screen.blit(logOutUser, (245, 4)) 
            pygame.display.update(drawing)

## TCP client that handles the connection to the Java algorithm server.
def socketClient():
    #use the global clientSocket variable
    global clientSocket
    
    #set the socket connection info
    host = "localhost"
    port = 5656
    
    #connect to the server with our client socket
    clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    clientSocket.connect((host, port))

## Sends quit message to server and closes socket instance
def socketQuit():
    #use the global clientSocket variable
    global clientSocket

    clientSocket.sendall("quit")
    clientSocket.close()

## Main method that drives the program.
if __name__ == "__main__":
    usernamefound = ""
    while len(usernamefound) < 1:
        execfile('login.py')
        usernamefound = "yes"
    global userid
    socketClient()
    sendUserId(userid)
    keyboard()

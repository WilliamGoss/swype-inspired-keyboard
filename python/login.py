import pygame
import sys
import socket
import json
from math import ceil
import MySQLdb
#import numpy as np
#import pandas

## Global variable for word choices
screen = pygame.display.set_mode((320, 480))
my_string = ""
p_string = ""
userWord = True
shiftCheck = False
userid = 0
    
## Prints the value of a single key when pressed.
def printSingleKey(key):
    global my_string
    global p_string
    global userWord
    global shiftCheck
    if shiftCheck:
        key = key.upper()
        shiftCheck = False
    if userWord:
        my_string += key
    else:
        p_string += key

## Word wrapper
def render_usertext(string, font, rect, text_color, background_color, justification=0): 
    final_lines = []

    requested_lines = string.splitlines()

    global screen

    # Create a series of lines that will fit on the provided
    # rectangle.

    for requested_line in requested_lines:
        if font.size(requested_line)[0] > rect.width:
            words = requested_line.split(' ')
            # if any of our words are too long to fit, return.
            for word in words:
                if font.size(word)[0] >= rect.width:
                    print ("The word " + word + " is too long to fit in the rect passed.")
            # Start a new line
            accumulated_line = ""
            for word in words:
                test_line = accumulated_line + word + " "
                # Build the line while the words fit.    
                if font.size(test_line)[0] < rect.width:
                    accumulated_line = test_line 
                else: 
                    final_lines.append(accumulated_line) 
                    accumulated_line = word + " " 
            final_lines.append(accumulated_line)
        else: 
            final_lines.append(requested_line) 

    accumulated_height = 0 
    for line in final_lines: 
        if accumulated_height + font.size(line)[1] >= rect.height:
            print ("Once word-wrapped, the text string was too tall to fit in the rect.")
        if line != "":
            tempsurface = font.render(line, 0, text_color)
            screen.blit(tempsurface, (45, accumulated_height + 113))
        accumulated_height += font.size(line)[1]

def render_passtext(string, font, rect, text_color, background_color, justification=0): 
    final_lines = []

    requested_lines = string.splitlines()

    global screen

    # Create a series of lines that will fit on the provided
    # rectangle.

    for requested_line in requested_lines:
        if font.size(requested_line)[0] > rect.width:
            words = requested_line.split(' ')
            # if any of our words are too long to fit, return.
            for word in words:
                if font.size(word)[0] >= rect.width:
                    print ("The word " + word + " is too long to fit in the rect passed.")
            # Start a new line
            accumulated_line = ""
            for word in words:
                test_line = accumulated_line + word + " "
                # Build the line while the words fit.    
                if font.size(test_line)[0] < rect.width:
                    accumulated_line = test_line 
                else: 
                    final_lines.append(accumulated_line) 
                    accumulated_line = word + " " 
            final_lines.append(accumulated_line)
        else: 
            final_lines.append(requested_line) 

    accumulated_height = 0 
    for line in final_lines: 
        if accumulated_height + font.size(line)[1] >= rect.height:
            print ("Once word-wrapped, the text string was too tall to fit in the rect.")
        if line != "":
            tempsurface = font.render(line, 0, text_color)
            screen.blit(tempsurface, (45, accumulated_height + 155))
        accumulated_height += font.size(line)[1]

def render_errortext(string, font, rect, text_color, background_color, justification=0): 
    final_lines = []

    requested_lines = string.splitlines()

    global screen

    # Create a series of lines that will fit on the provided
    # rectangle.

    for requested_line in requested_lines:
        if font.size(requested_line)[0] > rect.width:
            words = requested_line.split(' ')
            # if any of our words are too long to fit, return.
            for word in words:
                if font.size(word)[0] >= rect.width:
                    print ("The word " + word + " is too long to fit in the rect passed.")
            # Start a new line
            accumulated_line = ""
            for word in words:
                test_line = accumulated_line + word + " "
                # Build the line while the words fit.    
                if font.size(test_line)[0] < rect.width:
                    accumulated_line = test_line 
                else: 
                    final_lines.append(accumulated_line) 
                    accumulated_line = word + " " 
            final_lines.append(accumulated_line)
        else: 
            final_lines.append(requested_line) 

    accumulated_height = 0 
    for line in final_lines: 
        if accumulated_height + font.size(line)[1] >= rect.height:
            print ("Once word-wrapped, the text string was too tall to fit in the rect.")
        if line != "":
            tempsurface = font.render(line, 0, text_color)
            screen.blit(tempsurface, (65, accumulated_height + 15))
        accumulated_height += font.size(line)[1] 
    
## Handles pygame and the keyboard input.
def login_keyboard():
    #initialize pygame
    pygame.init()
    
    #make the window with the keyboard background
    #width, height = 640, 960
    #screen = pygame.display.set_mode((width, height))
    global screen
    background = pygame.image.load("../img/login-half-final.png").convert()

    #transparency boxes
    keyRectSize = (27, 42)
    rect = pygame.Surface(keyRectSize, pygame.SRCALPHA, 32)
    rect.fill((0, 0, 0, 0))
    spaceRect = pygame.Surface((156, 42), pygame.SRCALPHA, 32)
    spaceRect.fill((0, 0, 0, 0))
    bigkeyRectSize = (42, 42)
    bigKeyRect = pygame.Surface(bigkeyRectSize, pygame.SRCALPHA, 32)
    bigKeyRect.fill((0, 0, 0, 0))
    inputBoxSize = pygame.Surface((291, 39), pygame.SRCALPHA, 32)
    inputBoxSize.fill((0, 0, 0, 0))
    loginKeySize = (296, 38)
    loginBoxRect = pygame.Surface(loginKeySize, pygame.SRCALPHA, 32)
    loginBoxRect.fill((0, 0, 0, 0))
    
    #draw the keyboard background
    screen.blit(background, (0,0))
    
    #holds a dictionary containing each key on the keyboard and its coordinates for generating ideal sokgraphs
    keyRectCoords = {}
    
    #add the coordinates of all key rectangles to the dictonary of locations
    keyRectCoords['q'] = (5, 272)
    keyRectCoords['w'] = (37, 272)
    keyRectCoords['e'] = (68, 272)
    keyRectCoords['r'] = (99, 272)
    keyRectCoords['t'] = (131, 272)
    keyRectCoords['y'] = (162, 272)
    keyRectCoords['u'] = (194, 272)
    keyRectCoords['i'] = (225, 272)
    keyRectCoords['o'] = (257, 272)
    keyRectCoords['p'] = (288, 272)
    keyRectCoords['a'] = (21, 324)
    keyRectCoords['s'] = (53, 324)
    keyRectCoords['d'] = (84, 324)
    keyRectCoords['f'] = (115, 324)
    keyRectCoords['g'] = (147, 324)
    keyRectCoords['h'] = (178, 324)
    keyRectCoords['j'] = (210, 324)
    keyRectCoords['k'] = (241, 324)
    keyRectCoords['l'] = (273, 324)
    keyRectCoords['z'] = (52, 377)
    keyRectCoords['x'] = (83, 377)
    keyRectCoords['c'] = (115, 377)
    keyRectCoords['v'] = (147, 377)
    keyRectCoords['b'] = (179, 377)
    keyRectCoords['n'] = (211, 377)
    keyRectCoords['m'] = (241, 377)
    keyRectCoords['space'] = (82, 429)
    keyRectCoords['backspace'] = (273, 377)
    keyRectCoords['shift'] = (6, 377)
    keyRectCoords['username'] = (11, 103)
    keyRectCoords['password'] = (11, 150)
    keyRectCoords['login'] = (12, 215)
    
    #draw all of the key boxes
    q = screen.blit(rect, keyRectCoords['q'])
    w = screen.blit(rect, keyRectCoords['w'])
    e = screen.blit(rect, keyRectCoords['e'])
    r = screen.blit(rect, keyRectCoords['r'])
    t = screen.blit(rect, keyRectCoords['t'])
    y = screen.blit(rect, keyRectCoords['y'])
    u = screen.blit(rect, keyRectCoords['u'])
    i = screen.blit(rect, keyRectCoords['i'])
    o = screen.blit(rect, keyRectCoords['o'])
    p = screen.blit(rect, keyRectCoords['p'])
    a = screen.blit(rect, keyRectCoords['a'])
    s = screen.blit(rect, keyRectCoords['s'])
    d = screen.blit(rect, keyRectCoords['d'])
    f = screen.blit(rect, keyRectCoords['f'])
    g = screen.blit(rect, keyRectCoords['g'])
    h = screen.blit(rect, keyRectCoords['h'])
    j = screen.blit(rect, keyRectCoords['j'])
    k = screen.blit(rect, keyRectCoords['k'])
    l = screen.blit(rect, keyRectCoords['l'])
    z = screen.blit(rect, keyRectCoords['z'])
    x = screen.blit(rect, keyRectCoords['x'])
    c = screen.blit(rect, keyRectCoords['c'])
    v = screen.blit(rect, keyRectCoords['v'])
    b = screen.blit(rect, keyRectCoords['b'])
    n = screen.blit(rect, keyRectCoords['n'])
    m = screen.blit(rect, keyRectCoords['m'])
    space = screen.blit(spaceRect, keyRectCoords['space'])
    backspace = screen.blit(bigKeyRect, keyRectCoords['backspace'])
    shift = screen.blit(bigKeyRect, keyRectCoords['shift'])
    uname = screen.blit(inputBoxSize, keyRectCoords['username'])
    pword = screen.blit(inputBoxSize, keyRectCoords['password'])
    login = screen.blit(loginBoxRect, keyRectCoords['login'])

    my_font = pygame.font.Font("../misc/open-sans/OpenSans-Regular.ttf", 14)
    #string that is held in the username box
    global my_string
    my_rect = pygame.Rect((40, 40, 260, 26))
    rendered_utext = render_usertext(my_string, my_font, my_rect, (0, 0, 0), (0, 0, 0), 0)

    #string that is held in the password box
    global p_string
    my_rect = pygame.Rect((40, 40, 260, 26))
    rendered_ptext = render_passtext(p_string, my_font, my_rect, (0, 0, 0), (0, 0, 0), 0)

    #flag to hold which input box is selected
    global userWord
    #flag to hold shift status
    global shiftCheck

    #uncomment to generate ideal for words.txt (do not delete this line)
    #getWordListAndMakeIdealSokgraphs(keyRectCoords, keyRectSize)
    
    pygame.display.update()
    
    #mouse down position
    mdpos = None
    
    while True:
        xaxis, yaxis = pygame.mouse.get_pos()
        pygame.display.flip()
        
        for event in pygame.event.get():
            drawing = []
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                dpos = pygame.mouse.get_pos()
                mdpos = event.pos
                if uname.collidepoint(dpos):
                    userWord = True
                if pword.collidepoint(dpos):
                    userWord = False
            elif event.type == pygame.MOUSEBUTTONUP:
                upos = pygame.mouse.get_pos()
                mdpos = None
                screen.fill([0, 0, 0])
                screen.blit(background, (0,0))
                if q.collidepoint(dpos) and q.collidepoint(upos):
                    printSingleKey("q")
                if w.collidepoint(dpos) and w.collidepoint(upos):
                    printSingleKey("w")
                if e.collidepoint(dpos) and e.collidepoint(upos):
                    printSingleKey("e")
                if r.collidepoint(dpos) and r.collidepoint(upos):
                    printSingleKey("r")
                if t.collidepoint(dpos) and t.collidepoint(upos):
                    printSingleKey("t")
                if y.collidepoint(dpos) and y.collidepoint(upos):
                    printSingleKey("y")
                if u.collidepoint(dpos) and u.collidepoint(upos):
                    printSingleKey("u")
                if i.collidepoint(dpos) and i.collidepoint(upos):
                    printSingleKey("i")
                if o.collidepoint(dpos) and o.collidepoint(upos):
                    printSingleKey("o")
                if p.collidepoint(dpos) and p.collidepoint(upos):
                    printSingleKey("p")
                if a.collidepoint(dpos) and a.collidepoint(upos):
                    printSingleKey("a")
                if s.collidepoint(dpos) and s.collidepoint(upos):
                    printSingleKey("s")
                if d.collidepoint(dpos) and d.collidepoint(upos):
                    printSingleKey("d")
                if f.collidepoint(dpos) and f.collidepoint(upos):
                    printSingleKey("f")
                if g.collidepoint(dpos) and g.collidepoint(upos):
                    printSingleKey("g")
                if h.collidepoint(dpos) and h.collidepoint(upos):
                    printSingleKey("h")
                if j.collidepoint(dpos) and j.collidepoint(upos):
                    printSingleKey("j")
                if k.collidepoint(dpos) and k.collidepoint(upos):
                    printSingleKey("k")
                if l.collidepoint(dpos) and l.collidepoint(upos):
                    printSingleKey("l")
                if z.collidepoint(dpos) and z.collidepoint(upos):
                    printSingleKey("z")
                if x.collidepoint(dpos) and x.collidepoint(upos):
                    printSingleKey("x")
                if c.collidepoint(dpos) and c.collidepoint(upos):
                    printSingleKey("c")
                if v.collidepoint(dpos) and v.collidepoint(upos):
                    printSingleKey("v")
                if b.collidepoint(dpos) and b.collidepoint(upos):
                    printSingleKey("b")
                if n.collidepoint(dpos) and n.collidepoint(upos):
                    printSingleKey("n")
                if m.collidepoint(dpos) and m.collidepoint(upos):
                    printSingleKey("m")
                if space.collidepoint(dpos) and space.collidepoint(upos):
                    printSingleKey(" ")
                if shift.collidepoint(dpos) and shift.collidepoint(upos):
                    shiftCheck = True
                if backspace.collidepoint(dpos) and backspace.collidepoint(upos):
                    if userWord:
                        if len(my_string) > 0:
                            my_string = my_string[:-1]
                    else:
                        if len(p_string) > 0:
                            p_string = p_string[:-1]
                if login.collidepoint(dpos) and login.collidepoint(upos):
                    db = MySQLdb.connect(host="54.84.205.6", user="csc398", passwd="team4shark", db="shark2")
                    cur = db.cursor()
                    sql = "SELECT * from users \
                           WHERE USERNAME = '%s' AND UNHASHED_PASSWORD = '%s'" % (my_string, p_string)
                    cur.execute(sql)
                    sqlresult = cur.fetchall()
                    if len(sqlresult):
                        global userid
                        userid = str(sqlresult[0])[1]
                        return "Battle Cruisers Operational"
                    else:
                        e_string = "Invalid Username/Password"
                        rendered_etext = render_errortext(e_string, my_font, my_rect, (255, 20, 20), (0, 0, 0), 0) 
            rendered_utext = render_usertext(my_string, my_font, my_rect, (0, 0, 0), (0, 0, 0), 0)       
            rendered_ptext = render_passtext(p_string, my_font, my_rect, (0, 0, 0), (0, 0, 0), 0)       
            pygame.display.update(drawing)


## Main method that drives the program.
if __name__ == "__main__":
    login_keyboard()
